using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AspNetCoreWebApiCustomAuth.Controllers {
    [ApiController]
    [Route("auth")]
    public class AuthenticationController : ControllerBase {
        private readonly AuthenticationService service;

        public AuthenticationController(AuthenticationService service) {
            this.service = service;
        }

        [HttpPost]
        [Route("")]
        public IActionResult Login([FromBody] LoginDto dto) {
            var token = service.GenerateToken(dto.Username, dto.Password);
            if (token == null)
                return Unauthorized();
            return Ok(new { token });
        }
    }

    public record LoginDto(string Username, string Password);

    public class AuthenticationService {
        private static readonly IDictionary<string, string> Users = new Dictionary<string, string> {
            { "pietrom", "passw0rd" },
            { "jacekw", "p4ssword" }
        };
        
        private static readonly IDictionary<string, string[]> Roles = new Dictionary<string, string[]> {
            { "pietrom", new[] { "role0", "role1" }},
            { "jacekw", new[] { "role0", "role2" }},
        };

        public string GenerateToken(string username, string password) {
            if (Users.ContainsKey(username) && Users[username] == password) {
                var handler = new JwtSecurityTokenHandler();
                var descriptor = new SecurityTokenDescriptor {
                    Subject = new ClaimsIdentity(new Claim[] {
                      new Claim(ClaimTypes.Email, $"{username}@example.com"),
                      new Claim(ClaimTypes.Country, "Italy"),
                      new Claim(ClaimTypes.UserData, @"{""xx"": 11, ""yy"":19}")
                    }.Concat(Roles[username].Select(x => new Claim(ClaimTypes.Role, x)))),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(AuthConfig.TokenKeyBytes), SecurityAlgorithms.HmacSha256Signature),
                    Expires = DateTime.Now.AddHours(2)
                };
                var token = handler.CreateToken(descriptor);
                return handler.WriteToken(token);
            }
            return null;
        }
    }

    internal static class AuthConfig {
        public const string TokenKey = "unachiavequalunque";
        public static readonly byte[] TokenKeyBytes = Encoding.ASCII.GetBytes(TokenKey);
    }
}