﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AspNetCoreWebApiCustomAuth.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
        
        [HttpGet]
        [Route("v2")]
        [Authorize(Roles = "role0")]
        public IEnumerable<WeatherForecast> Get2() {
            return Get();
        }
        
        [HttpGet]
        [Route("v3")]
        [Authorize(Roles = "role1")]
        public IEnumerable<WeatherForecast> Get3() {
            return Get();
        }
        
        [HttpGet]
        [Route("v4")]
        [Authorize(Roles = "role2")]
        public IEnumerable<WeatherForecast> Get4() {
            return Get();
        }
    }
}
